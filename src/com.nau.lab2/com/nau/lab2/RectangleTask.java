package com.nau.lab2;

import javafx.application.Platform;
import javafx.scene.text.Text;

import java.util.Random;

public class RectangleTask implements Runnable {
    private final CustomRectangle rectangle;
    private final Text text;
    private double square;

    public RectangleTask(CustomRectangle rec, Text t) {
        rectangle = rec;
        text = t;
    }

    private byte getRandomMilliseconds() {
        return (byte) (new Random().nextInt(120) + 1);
    }

    public double getSquare() {
        return square;
    }

    @Override
    public void run() {
        byte timeout = getRandomMilliseconds();
        Runnable updater = () -> {
            rectangle.pulse();
            double width = rectangle.getWidth();
            double height = rectangle.getHeight();
            square = (double) Math.round(width * height * 100) / 100;
            text.setText("" + square);
        };
        while (true) {
            Platform.runLater(updater);
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                System.out.println("Process is interrupted");
            }
        }
    }

}
