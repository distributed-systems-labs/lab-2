package com.nau.lab2;

import javafx.application.Platform;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class ResultTask implements Runnable {
    private final Text text;
    private final ArrayList<RectangleTask> rectangleList;

    public ResultTask(Text t, ArrayList<RectangleTask> recTasks) {
        text = t;
        rectangleList = recTasks;
    }

    @Override
    public void run() {
        Runnable updater = () -> {
            double squareSum = 0.0;
            for (RectangleTask task : rectangleList) {
                squareSum += task.getSquare();
            }
            text.setText("Общая площадь: " + squareSum + " пикселов");
        };

        while (true) {
            Platform.runLater(updater);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("Process is interrupted");
            }
        }
    }
}
