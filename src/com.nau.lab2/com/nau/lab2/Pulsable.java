package com.nau.lab2;

public interface Pulsable {
    public void pulse();
}
