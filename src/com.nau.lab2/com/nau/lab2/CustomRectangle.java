package com.nau.lab2;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class CustomRectangle extends Rectangle implements Pulsable {
    public static final double WIDTH = 50;
    public final double MAX_HEIGHT = 140;
    private Direction direction = Direction.UP;

    public CustomRectangle(double posX, double posY, Color color) {
        super(posX, posY, WIDTH, 0.0);
        this.setFill(color);
    }

    @Override
    public void pulse() {
        if (this.direction == Direction.DOWN) {
            this.setHeight(this.getHeight() - 1);
        }
        if (this.direction == Direction.UP) {
            this.setHeight(this.getHeight() + 1);
        }
        if (this.getHeight() >= this.MAX_HEIGHT) this.direction = Direction.DOWN;
        if (this.getHeight() <= 0) this.direction = Direction.UP;
    }
}
